# Climaenelcerro mastodon bot

A bot that replies to queries about weather ⛅ in the mountains 🏔️ of the world
developed in rust.

## Development

Of course you need [rust](https://rustup.rs). Clone the code somewhere and enter
the directory. There you will need a `settings.toml` file with contents like
this:

```toml
#settings.toml
access_token = "yourtoken"
api_endpoint = "https://mstdn.mx/api/v1"
bot_name = "@climaenelcerro"
```

The token can be obtained by registering an account in some mastodon server and
then going to settings > development and creating a new application.

With this set up you can run the bot with:

    cargo run -- --config settings.toml

Then go and mention your bot to see what happens.
