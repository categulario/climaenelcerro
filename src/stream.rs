use std::time::Duration;

use futures::TryStreamExt;
use eventsource_client::{
    Client, ClientBuilder, ReconnectOptions, SSE, Error as ESError,
};

use crate::models::Notification;
use crate::clean::clean_content;
use crate::settings::Settings;

pub async fn run_stream(settings: &Settings, mountain_list: &[Mountain]) -> Result<(), ESError> {
    let access_token = &settings.access_token;
    let api_endpoint = &settings.api_endpoint;
    let client = ClientBuilder::for_url(&format!("{api_endpoint}/streaming/user/notification"))?
        .header("Authorization", &format!("Bearer {access_token}"))?
        .reconnect(
            ReconnectOptions::reconnect(true)
                .retry_initial(false)
                .delay(Duration::from_secs(1))
                .backoff_factor(2)
                .delay_max(Duration::from_secs(60))
                .build(),
        )
        .build();

    let mut stream = client.stream();
    let client = reqwest::Client::new();

    log::info!("Initiating listen loop");

    while let Some(event) = stream.try_next().await? {
        match event {
            SSE::Event(ev) if ev.event_type == "notification" => {
                let data = &ev.data;

                match serde_json::from_str(data) {
                    Ok(notif) => handle_event(notif, &client, settings).await,
                    Err(e) => log::error!("Error parsing notification: {e}"),
                }
            }
            SSE::Event(ev) => {
                let r#type = ev.event_type;
                log::debug!("unhandled event {type:?}");
            }
            SSE::Comment(_) => {}
        }
    }

    Ok(())
}

async fn handle_event(notif: Notification, client: &reqwest::Client, settings: &Settings) -> () {
    let r#type = notif.r#type;

    // bellow check that the status is not a reply so that the bot does not
    // reply to replies.
    if r#type == "mention" && notif.status.in_reply_to_id.is_none() {
        let content = clean_content(&notif.status.content, &settings);
        let api_endpoint = &settings.api_endpoint;
        let access_token = &settings.access_token;
        let acct = &notif.account.acct;

        // https://docs.joinmastodon.org/methods/statuses/#create
        log::debug!("Got mention from {acct} with text: {content}");
        let res = client.post(format!("{api_endpoint}/statuses"))
            .header("Authorization", format!("Bearer {access_token}"))
            .form(&notif.status.reply_with(format!("@{acct} escribió: {content}")))
            .send().await.unwrap();
        log::warn!("In production switch visibility to public");
        let code = res.status();

        if !code.is_success() {
            let text = res.text().await.unwrap();
            log::error!("trying to post status got {code} and text:\n{text}");
        }
    } else {
        log::debug!("Got notification type: {type}");
    }
}
