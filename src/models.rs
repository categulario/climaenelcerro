use serde::{Serialize, Deserialize};

#[derive(Debug, Deserialize)]
pub struct Notification {
    pub id: String,
    pub r#type: String,
    pub status: Status,
    pub account: Account,
}

#[derive(Debug, Deserialize)]
pub struct Status {
    pub id: String,
    pub content: String,
    pub in_reply_to_id: Option<String>,
}

impl Status {
    pub fn reply_with(&self, text: String) -> StatusRequest {
        StatusRequest {
            status: text,
            in_reply_to_id: Some(self.id.to_owned()),
            visibility: String::from("unlisted"),
        }
    }
}

#[derive(Debug, Serialize)]
pub struct StatusRequest {
    status: String,
    in_reply_to_id: Option<String>,
    visibility: String,
}

#[derive(Debug, Deserialize)]
pub struct Account {
    pub acct: String,
}
