use thiserror::Error;
use eventsource_client::Error as ESError;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Could not parse json: {0}")]
    Parse(serde_json::Error),

    #[error("Error in event source: {0:?}")]
    EventSource(ESError),

    #[error("Error reading settings: {0}")]
    ReadingSettings(std::io::Error),

    #[error("Error parsing settings: {0}")]
    ParsingSettings(toml::de::Error),

    #[error("Error reading mountain list: {0}")]
    ReadingMountains(std::io::Error),

    #[error("Error parsing mountain list: {0}")]
    ParsingMountains(serde_json::Error),
}

pub type Result<T> = std::result::Result<T, Error>;
