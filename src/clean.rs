use lazy_static::lazy_static;
use regex::Regex;

use crate::settings::Settings;

pub fn clean_content(s: &str, settings: &Settings) -> String {
    lazy_static! {
        static ref TAG_RE: Regex = Regex::new("<[^>]*>").unwrap();
    }

    TAG_RE.replace_all(s, "").replace(&settings.bot_name, "").trim().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn removes_all_tags() {
        assert_eq!(clean_content(r#"<p>Sospecho que tendré unas estimulantes conversaciones con <span class="h-card"><a href="https://mstdn.mx/@climaenelcerro" class="u-url mention">@<span>climaenelcerro</span></a></span></p>"#, &Default::default()), "Sospecho que tendré unas estimulantes conversaciones con");
    }
}
