use serde::Deserialize;

#[derive(Deserialize)]
pub struct Settings {
    pub bot_name: String,
    pub access_token: String,
    pub api_endpoint: String,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            bot_name: String::from("@climaenelcerro"),
            access_token: String::new(),
            api_endpoint: String::from("https://mstdn.mx/api/v1"),
        }
    }
}
