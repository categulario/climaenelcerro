// check https://docs.joinmastodon.org/methods/streaming/
use std::path::{PathBuf, Path};
use std::fs::read_to_string;

use clap::Parser;

use climaenelcerro::stream::run_stream;
use climaenelcerro::error::{Error, Result};
use climaenelcerro::settings::Settings;
use climaenelcerro::models::Mountain;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long, value_name="FILE")]
    config: PathBuf,

    #[clap(short, long, value_name="FILE")]
    mountains: PathBuf,
}

fn read_settings(filename: &Path) -> Result<Settings> {
    let contents = read_to_string(filename).map_err(|e| Error::ReadingSettings(e))?;

    toml::from_str(&contents).map_err(|e| Error::ParsingSettings(e))
}

fn read_mountain_list(filename: &Path) -> Result<Vec<Mountain>> {
    let contents = read_to_string(filename).map_err(|e| Error::ReadingMountains(e))?;

    serde_json::from_str(&contents).map_err(|e| Error::ParsingMountains(e))
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::builder()
        .filter(Some("climaenelcerro"), log::LevelFilter::Debug)
        .filter(Some("eventsource_client"), log::LevelFilter::Info)
        .format_timestamp(None)
        .init();

    let args = Args::parse();
    let settings = read_settings(&args.config)?;
    let mountain_list = read_mountain_list(&args.mountains)?;

    run_stream(&settings, &mountain_list).await.map_err(|e| Error::EventSource(e))
}
