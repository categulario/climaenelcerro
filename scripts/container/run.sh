#!/bin/bash

__dir=$(dirname $(realpath $0))

container=climaenelcerro
image=registry.gitlab.com/categulario/climaenelcerro/climaenelcerro

if [[ -z $@ ]]; then
    cmd=$image
    name="--name=$container"

    podman rm -i $container
else
    cmd="-it $image $@"
fi

podman run --rm -i -t \
    --uidmap 900:0:1 --uidmap 0:1:900 \
    --volume $__dir/../../settings.toml:/etc/climaenelcerro/settings.toml:U \
    $name \
    $cmd
